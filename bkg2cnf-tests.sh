#!/bin/sh

`ghc bkg-2-cnf.hs`
`chmod +x bkg-2-cnf`

for f in `ls in/ `
	do
	for i in {"i","1","2"}
	do
		out=`./bkg-2-cnf -$i in/$f | grep -o . | sort`
		ref=` cat out/${f}_${i}| grep -o . | sort`
		if [ "$ref" == "$out" ]; then
			echo -e "\e[32mTEST "${f#*_}" INPUT -$i  PASSED\e[39m"
		else
			echo -e "\e[31mTEST "${f#*_}" INPUT -$i  FAILED\e[39m"
		fi
	done
done
